# bot_elmt

Bot Elmt is a web application gathering open timestamped data and dispatch them through different open formats.

It is written in PHP, JS/Bootstrap and use a MariaDB or MySQL database.

Actually, Bot Elmt collect OpenWeatherMap forecasts and publish them on web site, mastodon toots, ICS calendar et RSS feeds.

## Installation

- Install source to your web site root directory
- copy config/app.conf.sample.php to config/app.conf.php
- edit config/app.conf.php
- Load timezone info if not done on your database server : mysql_tzinfo_to_sql /usr/share/zoneinfo | mysql -u root mysql
- Create a MariaDB / MySQL database and create tables with /schema.sql

## Initialise environment

- Your site should work: http://your.site.tld/
- Load cities: http://your.site.tld/?/cron/update_cities&crontoken=APP_CRON_TOKEN ( replace APP_CRON_TOKEN with value in config/app.conf.php )
- Create Mastodon application: http://your.site.tld/?/cron/get_mastodon_client_params&crontoken=APP_CRON_TOKEN and follow instruction (if not work retry loading page)
- Test a self sended toot : http://your.site.tld/?/cron/mastodon_test&crontoken=APP_CRON_TOKEN
- Add a hourly cron task calling: http://your.site.tld/?/cron/post_forecast&crontoken=APP_CRON_TOKEN ( replace APP_CRON_TOKEN with value in config/app.conf.php )

## Existing bots

You can see how it works on these sites :

- https://bot.elmt.eu/

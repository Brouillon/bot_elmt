<?php

class owmap
{
	
	// Icon codes to UTF-8 Emojis
	public static $iconsWeather = array(
		// day
		'01d' => '🌞', //clear sky
		'02d' => '🌤', //few clouds
		'03d' => '🌥', //scattered clouds
		'04d' => '☁', //broken clouds 
		'09d' => '🌧', //shower rain 
		'10d' => '🌦', //rain
		'11d' => '🌩', //thunderstorm
		'13d' => '❄', //snow
		'50d' => '🌫', //mist
		// night
		'01n' => '🌞', //clear sky
		'02n' => '🌤', //few clouds
		'03n' => '🌥', //scattered clouds
		'04n' => '☁', //broken clouds 
		'09n' => '🌧', //shower rain 
		'10n' => '🌦', //rain
		'11n' => '🌩', //thunderstorm
		'13n' => '❄', //snow
		'50n' => '🌫', //mist
		/*
		'01n' => '🌝', //clear sky
		'02n' => '🌚', //few clouds
		'03n' => '☁', //scattered clouds
		'04n' => '☁', //broken clouds 
		'09n' => '🌧', //shower rain 
		'10n' => '🌧', //rain
		'11n' => '🌩', //thunderstorm
		'13n' => '❄', //snow
		'50n' => '🌫', //mist
		*/
	);
	
	public static $rWindDir = array('⬇️','↙️','⬅️','↖️','⬆️','↗️','➡️','↘️');
	public static $rWindDirTxt = array('N','NE','E','SE','S','SW','W','NW');
	
	/**
	 * return a forecast object for a city
	 */
	public static function get_forecast($city_id)
	{
		// db connection
		$db = db::getDB();
		$db->insert('SET time_zone = "UTC"');
		
		$dbforecast = $db->getFirst('SELECT * FROM forecasts WHERE id=?',
						array($city_id));
						
		if($dbforecast===false)
		{
			$dbforecast = new dbObj('forecasts');
			$dbforecast->id = $city_id;
		}
		
		if(!isset($dbforecast->updated_at) || $dbforecast->updated_at < date('Y-m-d H:i:s', time()-APP_FORECAST_VALIDITY))
		{
			// Set timeout
			$ctx = stream_context_create(array(
				'http'=>
					array(
						'timeout' => 300,  // 300 seconds
					)
			));
			for($occurs=0; $occurs<4; $occurs++)
			{
				$json = @file_get_contents(APP_OWM_FORECAST_URL."?units=metric&lang=en&id=".$city_id."&APPID=".APP_OWM_API_APPID, false, $ctx);
				if($json===false || strlen($json)<10000)
					sleep(2);
				else
					break;
			}
			if($json!==false && strlen($json)>=10000)
			{
				$dbforecast->forecast_json = $json;
				$dbforecast->update();
			}
			
		}
		
		if(!isset($dbforecast->forecast_json))
			return false;
		
		return json_decode($dbforecast->forecast_json);
	}
	
	/**
	 * convert units 
	 * @param $forecast : forecast object
	 * @param $temp : temperature unit ('K', 'C' or 'F')
	 * @param $speed : wind speed unit ('m/s', 'km/h' or 'miles/hour')
	 * @param $lang : language code ('en', 'fr', 'es'....)
	 * @param $timezone : timezone name ('UTC', 'Europe/Paris'....)
	 */
	public static function forecast_set_units($forecast, $temp, $speed, $lang, $timezone)
	{
		$DTtzUser = new DateTimeZone($timezone);
		
		$rWeatherCond = get_type_translations('weather', $lang);
		$rDays = get_type_translations('days', $lang);

		foreach($forecast->list as &$aForecast)
		{
			// date time
			$localdt = new DateTime(date("Y-m-d H:i:s",$aForecast->dt));
			// User TZ
			$localdt->setTimezone($DTtzUser);
			$aForecast->dt_local_short = $localdt->format('D H:i');
			$aForecast->dt_local_short_day = $rDays[$localdt->format('N')];
			$aForecast->dt_local_short_time = $localdt->format('H:i');
			$aForecast->dt_local_long = $localdt->format('Y-m-d H:i:s');
			$aForecast->dt_local_long_day = $aForecast->dt_local_short_day . $localdt->format(' d');
			$aForecast->dt_local_date = $localdt->format('Y-m-d');
			$aForecast->dt_local_8601 = $localdt->format('c');
			$aForecast->dt_local_2822 = $localdt->format('r');
			// UTC
			$aForecast->dt_utc_ics_from = gmdate("Ymd\THis\Z",$aForecast->dt);
			$aForecast->dt_utc_ics_to = gmdate("Ymd\THis\Z",$aForecast->dt+300);
			
			$aForecast->dt_timezone = $timezone;
			// t°
			switch($temp)
			{
				case 'K':
					$aForecast->main->temp = round($aForecast->main->temp + 273.15).' K';
					$aForecast->main->temp_min = round($aForecast->main->temp_min + 273.15).' K';
					$aForecast->main->temp_max = round($aForecast->main->temp_max + 273.15).' K';
				break;
				case 'C':
					$aForecast->main->temp = round($aForecast->main->temp).'°C';
					$aForecast->main->temp_min = round($aForecast->main->temp_min).'°C';
					$aForecast->main->temp_max = round($aForecast->main->temp_max).'°C';
				break;
				case 'F':
					$aForecast->main->temp = round($aForecast->main->temp*1.8+32).'°F';
					$aForecast->main->temp_min = round($aForecast->main->temp_min*1.8+32).'°F';
					$aForecast->main->temp_max = round($aForecast->main->temp_max*1.8+32).'°F';
				break;
			}
			// rain
			if(isset($aForecast->rain->{'3h'}) && round($aForecast->rain->{'3h'})>0)
				$aForecast->rain->volume = round($aForecast->rain->{'3h'}).'mm';
			else
			{
				if(!isset($aForecast->rain))
					$aForecast->rain = new stdClass();
				$aForecast->rain->volume = '';
			}
			// wind speed
			switch($speed)
			{
				case 'm/s':
					$aForecast->wind->speed = $aForecast->wind->speed.' m/s';
				break;
				case 'km/h':
					$aForecast->wind->speed = round($aForecast->wind->speed*3.6).' km/h';
				break;
				case 'miles/hour':
					$aForecast->wind->speed = round($aForecast->wind->speed*2.236936292).' mi/h';
				break;
			}
			$aForecast->wind->short_direction = round($aForecast->wind->deg/45)%8;
			$aForecast->wind->emoji = self::$rWindDir[$aForecast->wind->short_direction];
			$aForecast->wind->dirtxt = self::$rWindDirTxt[$aForecast->wind->short_direction];
			$aForecast->wind->img = APP_SITE_URL.'/img/wind/'.$aForecast->wind->short_direction.'.png';
			
			// weather condition translation
			foreach($aForecast->weather as &$weather)
			{
				$weather->local_description = $rWeatherCond['cond_'.$weather->id];
				$weather->emoji = self::$iconsWeather[$weather->icon];
				$weather->img = APP_SITE_URL.'/img/weather/'.$weather->icon.'.png';
			}
			
		}
		return $forecast;
	}
	
	public static function forecast_format_mastodon($forecast)
	{
		
		return $html;
	}

}

<?php

class db extends PDO
{
	private static $s_db = false;

	public static function getDB()
	{
		if(self::$s_db===false)
		{
			self::$s_db = new db(APP_DB_DSN, APP_DB_USR, APP_DB_PWD);
		}
		return self::$s_db;
	}

	public function getValue($sql, $bind=false)
	{
		if($bind!==false)
		{
			$query = $this->prepare($sql);
			$query->execute($bind);
		}
		else
		{
			$query = $this->query($sql);
		}

		if($query==false)
			return false;

		$first = $query->fetch(PDO::FETCH_NUM);
		if($first!==false)
		{
			$query->closeCursor();
			return $first[0];
		}

		return false;
	}

	public function getFirst($sql, $bind=false)
	{
		if(preg_match('/FROM\s([a-z0-9_]+)/i',$sql,$matches) && isset($matches[1]))
			$table = $matches[1];
		else
			$table = '';

		if($bind!==false)
		{
			$query = $this->prepare($sql);
			$query->execute($bind);
		}
		else
		{
			$query = $this->query($sql);
		}

		if($query==false)
			return false;

		$first = $query->fetchObject('dbObj', array($table));
		if($first!==false)
			$query->closeCursor();

		return $first;
	}

	public function getAll($sql, $bind=false)
	{
		if(preg_match('/FROM\s([a-z0-9_]+)/i',$sql,$matches) && isset($matches[1]))
			$table = $matches[1];
		else
			$table = '';

		if($bind!==false)
		{
			$query = $this->prepare($sql);
			$query->execute($bind);
		}
		else
		{
			$query = $this->query($sql);
		}

		if($query==false)
			return false;

		
		return $query->fetchAll(PDO::FETCH_CLASS, 'dbObj', array($table));
	}

	public function insert($sql, $bind=false)
	{
		if($bind!==false)
		{
			$query = $this->prepare($sql);
			$query->execute($bind);
		}
		else
		{
			$query = $this->exec($sql);
		}

		if($query==false)
			return false;

		return $this->lastInsertId();
	}

	public function insertOrUpdateObj($table, $obj)
	{
		if($table=='')
		{
			debug_print_backtrace();
			die('unknown table');
		}
		$values = get_object_vars($obj);

		$q = $this->query("DESCRIBE ".$table);
		$fields = $q->fetchAll(PDO::FETCH_COLUMN);

		if(in_array('updated_at', $fields))
			$values['updated_at'] = date('Y-m-d H:i:s');

		if(in_array('created_at', $fields) && !isset($values['created_at']))
			$values['created_at'] = date('Y-m-d H:i:s');

		foreach($fields as $field)
		{
			if(isset($values[ $field ]))
				$rFields[] = $field;
		}
		
		$sql = "INSERT INTO $table (";
		foreach($rFields as $field)
		{
			$sql.= "$field,";
		}
		$sql = substr($sql,0,-1);
		$sql.= ") VALUES (";
		foreach($rFields as $field)
		{
			$sql.= "?,";
		}
		$sql = substr($sql,0,-1);
		$sql.= ") ON DUPLICATE KEY UPDATE ";
		foreach($rFields as $field)
		{
			$sql.= "$field=?,";
		}
		$sql = substr($sql,0,-1);

		$bind = array();
		foreach($rFields as $field)
		{
			if(isset($values[$field]))
				$bind[] = $values[$field];
		}
		foreach($rFields as $field)
		{
			if(isset($values[$field]))
				$bind[] = $values[$field];
		}

		$query = $this->prepare($sql);
		$query->execute($bind);

		if($query==false)
			return false;

		$iLastId = $this->lastInsertId();
		if($iLastId!==false)
			return $iLastId;

		$first = $query->fetch(PDO::FETCH_NUM);
		if($first!==false)
		{
			$query->closeCursor();
			return $first[0];
		}

		return false;
	}
}

class dbObj extends StdClass
{
	private $m__table;
	
	function __construct($table)
	{
		$this->m__table = $table;
	}
	
	function update()
	{
		$db = db::getDB();
		return $db->insertOrUpdateObj($this->m__table, $this);
	}
	
	function touch()
	{
		if(!isset($this->id) || intval($this->id)<=0)
			return false;
		
		$db = db::getDB();
		return $db->exec('UPDATE '.$this->m__table.' SET updated_at=NOW() WHERE id='.intval($this->id));
	}
	
}

<?php

class Main extends Controller
{
	/**
	 * 
	 */
	function index($args)
	{
		$data=array();
		$data['title'] = t('Home_page');
		// db connection
		$db = db::getDB();
		$db->insert('SET time_zone = "UTC"');
		
		$redir = $db->getFirst("SELECT c.* FROM forecasts f, cities c
								WHERE c.id=f.id ORDER BY f.created_at DESC");

		if($redir!==false)
			$data['refresh_url'] = APP_SITE_URL.'/?/main/forecast/'.$redir->id.'/'.txt::url($redir->name);
		
		return $data;
	}
	
	/**
	 * Forecast 
	 * @params $args[0] = city id
	 * @params $args[1] = city name
	 */
	function forecast($args)
	{
		$data=array();
		
		if(isset($args['unsub']))
		{
			$data['unsub'] = $args['unsub'];
		}
		
		$rTimezones = DateTimeZone::listIdentifiers();
		
		if(!isset($_SESSION['unit_temp']) || $_SESSION['unit_temp']=='')
		{
			if(isset($_COOKIE['unit_temp']) && in_array($_COOKIE['unit_temp'], array('C','F','K')))
				$_SESSION['unit_temp']=$_COOKIE['unit_temp'];
			else
				$_SESSION['unit_temp']='C';
		}
		
		if(!isset($_SESSION['unit_speed']) || $_SESSION['unit_speed']=='')
		{
			if(isset($_COOKIE['unit_speed']) && in_array($_COOKIE['unit_speed'], array('m/s','km/h','miles/hour')))
				$_SESSION['unit_speed']=$_COOKIE['unit_speed'];
			else
				$_SESSION['unit_speed']='m/s';
		}
		
		if(!isset($_SESSION['timezone']) || $_SESSION['timezone']=='' || $_SESSION['timezone']=='UTC')
		{
			if(isset($_COOKIE['timezone']) && in_array($_COOKIE['timezone'], $rTimezones))
				$_SESSION['timezone']=$_COOKIE['timezone'];
			elseif(isset($_COOKIE['browser_tz']) && in_array(trim($_COOKIE['browser_tz']), $rTimezones))
				$_SESSION['timezone']=trim($_COOKIE['browser_tz']);
			else
				$_SESSION['timezone']='';
		}
			
		
		if(isset($_POST) && count($_POST)>0)
		{
			if(isset($_POST['unit_temp']) && in_array($_POST['unit_temp'], array('C','F','K')))
			{
				$_SESSION['unit_temp'] = $_POST['unit_temp'];
				setcookie('unit_temp', $_POST['unit_temp'], time()+3600*24*180);
			}
				
			if(isset($_POST['unit_speed']) && in_array($_POST['unit_speed'], array('m/s','km/h','miles/hour')))
			{
				$_SESSION['unit_speed'] = $_POST['unit_speed'];
				setcookie('unit_speed', $_POST['unit_speed'], time()+3600*24*180);
			}
				
			if(isset($_POST['timezone']) && in_array($_POST['timezone'], $rTimezones))
			{
				$_SESSION['timezone'] = $_POST['timezone'];
				setcookie('timezone', $_POST['timezone'], time()+3600*24*180);
			}
			
			redirect('main/forecast',$args);
		}

		// db connection
		$db = db::getDB();
		$db->insert('SET time_zone = "UTC"');
		
		$city_id=intval($args[0]);
		
		$data['city'] = $db->getFirst('SELECT * FROM cities WHERE id=?', array($city_id));
		if($data['city']===false)
			redirect('');

		
		if($_SESSION['timezone']=='')
		{
			// TODO: determine with city country, lat and lon the closest Timezone
			$_SESSION['timezone']='UTC';
		}
		
		$data['unit_temp'] = $_SESSION['unit_temp'];
		$data['unit_speed'] = $_SESSION['unit_speed'];
		$data['userlang'] = $_SESSION['userlang'];
		$data['timezone'] = $_SESSION['timezone'];
		$data['rTimezones'] = $rTimezones;
		
		$data['forecast'] = owmap::get_forecast($city_id);
		
		if($data['forecast']===false)
			redirect('');
		
		$data['forecast'] = owmap::forecast_set_units($data['forecast'], $data['unit_temp'], $data['unit_speed'], $data['userlang'], $data['timezone']);
		
		$data['title'] = t('Forecasts_for', $data['city']->name.' ['.$data['city']->country.'] ('.($data['city']->lat*1.0).','.($data['city']->lon*1.0).')');
		
		return $data;
	}
	
	/**
	 * Choose city modal form
	 * 
	 * @param $args[0] = prefilled country
	 * 
	 */
	function modal_city($args)
	{
		$data=array();
		// db connection
		$db = db::getDB();
		$db->insert('SET time_zone = "UTC"');
		
		$data['rCountries'] = get_type_translations('countries');
		
		if(isset($args[0]) && isset($data['rCountries']['c'.$args[0]]))
		{
			$data['country'] = $args[0];
		}
		
		if(isset($_POST) && count($_POST)>0)
		{

			if(isset($_POST['city']) && intval($_POST['city'])>0)
				$data['city'] = intval($_POST['city']);
			else
				$data['city'] = -1;

			if(isset($_POST['country']) && isset($data['rCountries']['c'.$_POST['country']]))
			{
				$data['country'] = $_POST['country'];
				
				if(isset($_POST['city_search']) && mb_strlen($_POST['city_search'])>3)
				{
					$data['city_search'] = $_POST['city_search'];
					$data['rCities'] = $db->getAll('SELECT * FROM cities WHERE (MATCH(name) AGAINST(? IN BOOLEAN MODE)  OR id=?) AND country=? LIMIT 50',
						array('+'.str_replace(array('-',' '),' +',$data['city_search']), $_POST['city'], $data['country']));
				}
			}
			
			if(isset($_POST['choose']) && intval($_POST['city'])>0)
			{
				$city_name = $db->getValue('SELECT name FROM cities WHERE id=?',array($data['city']));
				if($city_name!==false)
					redirect('main/modal_redirect/main/forecast/'.$data['city'].'/'.txt::url($city_name));
			}
		}
		
		return $data;
	}
	
	/**
	 * Subscribe to mastodon daily toot
	 * 
	 * @param $args[0] = city id
	 * @param $args[1] = temp unit
	 * @param $args[2] = speed unit (distance part)
	 * @param $args[3] = speed unit (time part)
	 * @param $args[4] = user lang
	 * @param $args[5] = timezone (continental part)
	 * @param $args[6] = timezone (city part)
	 * @param $args[7] = timezone (alt part)
	 * 
	 */
	function modal_mastodon($args)
	{
		$data=array();
		
		$data['city_id'] = intval($args[0]);
		$data['unit_temp'] = $args[1];
		$data['unit_speed'] = $args[2].'/'.$args[3];
		$data['userlang'] = $args[4];
		$data['timezone'] = $args[5];
		if(isset($args[6]))
			$data['timezone'] .= '/'.$args[6];
		if(isset($args[7]))
			$data['timezone'] .= '/'.$args[7];
			
		// db connection
		$db = db::getDB();
		$db->insert('SET time_zone = "UTC"');
		
		$data['city'] = $db->getFirst('SELECT * FROM cities WHERE id=?', array($data['city_id']));

		if($data['city']===false)
			redirect('main/modal_redirect/');
		
		
		if(isset($_POST) && count($_POST)>0)
		{
			$token=md5(date('YmdHis').$_SERVER["HTTP_USER_AGENT"]);
			
			$data['address'] = trim($_POST['address']);
			$data['local_time_range'] = intval($_POST['local_time_range']);

			if ( substr($data['address'],0,1)!='@' || filter_var(substr($data['address'],1), FILTER_VALIDATE_EMAIL)===false )
			{
				$data['err'] = t('Bad_address');
				return $data;
			}
			
			if ( $data['local_time_range']<0 || $data['local_time_range']>23 )
			{
				$data['err'] = t('Bad_time');
				return $data;
			}
			
			$prefs = array(
				'emojis'=>$_POST['pref_emojis']*1,
				'temperature'=>$_POST['pref_temperature']*1,
				'rain3h'=>$_POST['pref_rain3h']*1,
				'humidity'=>$_POST['pref_humidity']*1,
				'wind'=>$_POST['pref_wind']*1,
				);
			
			$res = $db->insert('INSERT INTO subscriptions (token, status, platform, preferences, city_id, lang,
							address, local_time_range,
							unit_temp, unit_speed, timezone, created_at, updated_at) 
							VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), NOW())',
							array($token, 'C', 'toot', json_encode($prefs), $data['city_id'], $data['userlang'],
							$data['address'], $data['local_time_range'], 
							$data['unit_temp'], $data['unit_speed'], $data['timezone']));
			if($res===false)
			{
				$data['err'] = 'Error while subscription creation '.print_r($_POST, true);
			}
			else
			{
				$data['msg'] = t('Toot_scheduled');
			}
		}
		
		return $data;
	}
	
	/**
	 * Subscribe to ics calendar
	 * 
	 * @param $args[0] = city id
	 * @param $args[1] = temp unit
	 * @param $args[2] = speed unit (distance part)
	 * @param $args[3] = speed unit (time part)
	 * @param $args[4] = user lang
	 * @param $args[5] = timezone (continental part)
	 * @param $args[6] = timezone (city part)
	 * @param $args[7] = timezone (alt part)
	 * 
	 */
	function modal_ics($args)
	{
		$data=array();
		
		$data['city_id'] = intval($args[0]);
		$data['unit_temp'] = $args[1];
		$data['unit_speed'] = $args[2].'/'.$args[3];
		$data['userlang'] = $args[4];
		$data['timezone'] = $args[5];
		if(isset($args[6]))
			$data['timezone'] .= '/'.$args[6];
		if(isset($args[7]))
			$data['timezone'] .= '/'.$args[7];
			
		// db connection
		$db = db::getDB();
		$db->insert('SET time_zone = "UTC"');
		
		$data['city'] = $db->getFirst('SELECT * FROM cities WHERE id=?', array($data['city_id']));

		if($data['city']===false)
			redirect('main/modal_redirect/');
		
		
		if(isset($_POST) && count($_POST)>0)
		{
			$token=md5(date('YmdHis').$_SERVER["HTTP_USER_AGENT"]);
			
			$prefs = array('format'=>$_POST['pref_format'], 'emojis'=>$_POST['pref_emojis']*1);
			
			$res = $db->insert('INSERT INTO subscriptions (token, status, platform, preferences, city_id, lang,
							unit_temp, unit_speed, timezone, created_at, updated_at) 
							VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), NOW())',
							array($token, 'V', 'ics', json_encode($prefs), $data['city_id'], $data['userlang'],
							$data['unit_temp'], $data['unit_speed'], $data['timezone']));
			if($res===false)
			{
				$data['err'] = 'Error while subscription creation '.print_r($_POST, true);
			}
			else
			{
				$data['msg'] = t('Calendar_created');
				$data['urlics'] = APP_SITE_URL.'/?/get/forecast/'.$token;
			}
		}
		
		return $data;
	}
	
	/**
	 * Subscribe to rss feed
	 * 
	 * @param $args[0] = city id
	 * @param $args[1] = temp unit
	 * @param $args[2] = speed unit (distance part)
	 * @param $args[3] = speed unit (time part)
	 * @param $args[4] = user lang
	 * @param $args[5] = timezone (continental part)
	 * @param $args[6] = timezone (city part)
	 * @param $args[7] = timezone (alt part)
	 * 
	 */
	function modal_rss($args)
	{
		$data=array();
		
		$data['city_id'] = intval($args[0]);
		$data['unit_temp'] = $args[1];
		$data['unit_speed'] = $args[2].'/'.$args[3];
		$data['userlang'] = $args[4];
		$data['timezone'] = $args[5];
		if(isset($args[6]))
			$data['timezone'] .= '/'.$args[6];
		if(isset($args[7]))
			$data['timezone'] .= '/'.$args[7];
			
		// db connection
		$db = db::getDB();
		$db->insert('SET time_zone = "UTC"');
		
		$data['city'] = $db->getFirst('SELECT * FROM cities WHERE id=?', array($data['city_id']));

		if($data['city']===false)
			redirect('main/modal_redirect/');
		
		
		if(isset($_POST) && count($_POST)>0)
		{
			$token=md5(date('YmdHis').$_SERVER["HTTP_USER_AGENT"]);
			
			$prefs = array('emojis'=>$_POST['pref_emojis']*1);
			
			$res = $db->insert('INSERT INTO subscriptions (token, status, platform, preferences, city_id, lang,
							unit_temp, unit_speed, timezone, created_at, updated_at) 
							VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), NOW())',
							array($token, 'V', 'rss', json_encode($prefs), $data['city_id'], $data['userlang'],
							$data['unit_temp'], $data['unit_speed'], $data['timezone']));
			if($res===false)
			{
				$data['err'] = 'Error while subscription creation '.print_r($_POST, true);
			}
			else
			{
				$data['msg'] = t('RSS_feed_created');
				$data['urlrss'] = APP_SITE_URL.'/?/get/forecast/'.$token;
			}
		}
		
		return $data;
	}

	/**
	 * Unsubscribe 
	 * 
	 * @param $args[0] = unsub token
	 * @param $args[1] = city id
	 * @param $args[2] = city name
	 */
	function modal_unsub($args)
	{
		$data=array();
		
		$data['token'] = trim($args[0]);
		$data['city_id'] = intval($args[1]);
		$data['city_name'] = $args[12];
		$data['unit_speed'] = $args[2].'/'.$args[3];
		$data['userlang'] = $args[4];
		$data['timezone'] = $args[5];
		if(isset($args[6]))
			$data['timezone'] .= '/'.$args[6];
		if(isset($args[7]))
			$data['timezone'] .= '/'.$args[7];
			
		// db connection
		$db = db::getDB();
		$db->insert('SET time_zone = "UTC"');
		
		$db->insert('UPDATE subscriptions SET status="X", updated_at=NOW() WHERE token=? AND city_id=?',
				array($data['token'], $data['city_id']));

		redirect('main/modal_redirect/main/forecast/'.$data['city_id'].'/'.txt::url($data['city_name'] ));
	}
	
	/**
	 * Change lang
	 *
	 */
	function modal_lang($args)
	{
		$data['msg'] = t('Good');
		$data['wrn'] = t('Oops');
		$data['err'] = t('Grrr');

		// db connection
		$db = db::getDB();
		$db->insert('SET time_zone = "UTC"');
		
		unset($data['msg']);
		unset($data['wrn']);
		unset($data['err']);

		$data=array();
		$data['args']=$args;
		
		if( isset($args[0]) ) 
		{
			if( !file_exists($_SERVER['DOCUMENT_ROOT'].'/lang/'.$args[0].'.txt') )
				$data['err'] = t('Translation_Not_found',$_SERVER['DOCUMENT_ROOT'].'/lang/'.$args[0].'.txt'.$args[0]);
			else
			{
				$_SESSION['userlang'] = $args[0];
				setcookie('userlang', $args[0], time()+3600*24*180);
				$data['msg'] = t('Welcome_In_Translation',$_SERVER['DOCUMENT_ROOT'].'/lang/'.$args[0].'.txt'.$args[0]);
			}
		}
		return $data;
	}
	
	/*
	 * Fenetre sortie du mode modal
	 * qui redirige vers la page $args[0]/$args[1]/$args[2]...
	 */
	function modal_redirect($args)
	{
		// Prepare data for view
		$data = array();

		$data['redirecturl'] = '?';
		$i = 0;
		while(isset($args[$i]))
			$data['redirecturl'] .= '/'.$args[$i++];

		return $data;
	}

}

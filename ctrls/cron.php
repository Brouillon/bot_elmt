<?php

class Cron extends Controller
{
	
	function __construct()
	{
		parent::__construct();
		
		header('Content-Type: text/plain; charset=UTF-8', true);
	}
	
	
	/**
	 * 
	 */
	function index($args)
	{
		return false;
	}


	/**
	 * Update cities list from OpenWeatherMap database
	 * 
	 * @param crontoken : APP_CRON_TOKEN (pass through url &crontoken=.......)
	 */
	function update_cities($args)
	{
		// check cron token
		if(!isset($args['crontoken']) || $args['crontoken'] != APP_CRON_TOKEN)
		{
			sleep(rand(2,10));
			die("Bad token ! see APP_CRON_TOKEN in config file.");
		}
		
		
		// db connection
		$db = db::getDB();
		$db->insert('SET time_zone = "UTC"');
		
		try
		{
			// get cities file
			@mkdir(APP_UPLOADS_DATA);
			$fp = fopen(APP_UPLOADS_DATA."city.list.json.gz", "wb");

			$ch = curl_init(APP_OWM_CITIES_URL);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_FILE, $fp);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
			curl_setopt($ch, CURLOPT_VERBOSE, true);
			
			curl_exec($ch);
			curl_close($ch);
			fclose($fp);
			
			// read cities
			$fc = gzopen(APP_UPLOADS_DATA."city.list.json.gz", 'r');
			$cities_json = gzread($fc, 100*1024*1024); // 100 Mb
			$cities = json_decode($cities_json);
			$cities_json=false;
			log::addlog(count($cities).' cities found', true);
			
			$stmt = $db->prepare('INSERT INTO `cities` 
									(`id`, `name`, `country`, `lon`, `lat`, `created_at`, `updated_at`)
									VALUES (?,?,?,?,?,NOW(),NOW())
									ON DUPLICATE KEY UPDATE
									`name`=?, `country`=?, `lon`=?, `lat`=?, `updated_at`=NOW()');
			foreach($cities as $city)
			{
				$stmt->execute(array($city->id, $city->name, $city->country, $city->coord->lon, $city->coord->lat, 
												$city->name, $city->country, $city->coord->lon, $city->coord->lat));
			}
		}
		catch(Exception $e)
		{
			log::addlog((string) $e, true);
			echo (string) $e;
		}
		
		return false;
	}
	
	
	/**
	 * Create mastodon app
	 * 
	 * @param crontoken : APP_CRON_TOKEN (pass through url &crontoken=.......)
	 */
	function get_mastodon_client_params($args)
	{
		// check cron token
		if(!isset($args['crontoken']) || $args['crontoken'] != APP_CRON_TOKEN)
		{
			sleep(rand(2,10));
			die("Bad token ! see APP_CRON_TOKEN in config file.");
		}
		
		$mastodon_api = new Mastodon_api();
		$mastodon_api->set_url(APP_MASTODON_INSTANCE_URL);
		
		$res = $mastodon_api->create_app(APP_MASTODON_APPNAME, null, null, APP_SITE_URL);
		if(!isset($res['html']['client_id']) || !isset($res['html']['client_secret']))
		{
			echo "Error during Mastodon app creation\n\nYou can retry by reloading this page\n\n";
			print_r($res);
			return false;
		}

		$res2 = $mastodon_api->login(APP_MASTODON_USER_EMAIL,APP_MASTODON_USER_PASSWORD);
		if(!isset($res2['html']['access_token']) || !isset($res2['html']['token_type']))
		{
			echo "Error during Mastodon login\n\nYou can retry by reloading this page\n\n";
			print_r($res2);
			return false;
		}

		echo "\nApp creation successfully\n\nYou can update these lines in config/app.conf.php :\n\n";
		
		echo "define('APP_MASTODON_CLIENT_ID', '".$res['html']['client_id']."');\n";
		echo "define('APP_MASTODON_CLIENT_SECRET', '".$res['html']['client_secret']."');\n";
		echo "define('APP_MASTODON_ACCESS_TOKEN', '".$res2['html']['access_token']."');\n";
		echo "define('APP_MASTODON_TOKEN_TYPE', '".$res2['html']['token_type']."');\n";
		
		return false;
	}
	
	/**
	 * Test mastodon app
	 * 
	 * @param crontoken : APP_CRON_TOKEN (pass through url &crontoken=.......)
	 */
	function mastodon_test($args)
	{
		// check cron token
		if(!isset($args['crontoken']) || $args['crontoken'] != APP_CRON_TOKEN)
		{
			sleep(rand(2,10));
			die("Bad token ! see APP_CRON_TOKEN in config file.");
		}
		
		$mastodon_api = new Mastodon_api();
		$mastodon_api->set_url(APP_MASTODON_INSTANCE_URL);
		
		$mastodon_api->set_client(APP_MASTODON_CLIENT_ID,APP_MASTODON_CLIENT_SECRET);
		$mastodon_api->set_token(APP_MASTODON_ACCESS_TOKEN,APP_MASTODON_TOKEN_TYPE);
		$res3 = $mastodon_api->accounts_verify_credentials();
		if(!isset($res3['html']['username']))
		{
			echo "Error during Mastodon get profile\n\n";
			print_r($res3);
			return false;
		}
		print_r($mastodon_api->post_statuses(array(
				'status' => "@".$res3['html']['username']." test from ".APP_MASTODON_APPNAME."\n\n".APP_SITE_URL,
				'visibility' => 'direct'
				)));
		
		return false;
	}
	
	/**
	 * Send hourly forecast
	 * 
	 * @param args[0] : subscription token (to force a toot, optional)
	 * @param crontoken : APP_CRON_TOKEN (pass through url &crontoken=.......)
	 */
	function post_forecast($args)
	{
		// check cron token
		if(!isset($args['crontoken']) || $args['crontoken'] != APP_CRON_TOKEN)
		{
			sleep(rand(2,10));
			die("Bad token ! see APP_CRON_TOKEN in config file.");
		}

		if(isset($args[0]) && strlen($args[0])>30)
			$token=$args[0];
		else
			$token=false;
		
		// db connection
		$db = db::getDB();
		$db->insert('SET time_zone = "UTC"');
		
		$sql = 'SELECT s.*, c.name city_name, c.country city_country FROM subscriptions s
						LEFT OUTER JOIN cities c ON (c.id = s.city_id)
						WHERE s.status="C" ';
								
		if($token===false)
			$subs = $db->getAll($sql.' AND CONVERT(SUBSTRING(CONVERT_TZ(NOW(), "UTC", timezone),12,2), INT)=local_time_range');
		else
			$subs = $db->getAll($sql.' AND s.token=?', array($token));
		
		if($subs===false || count($subs)==0)
			die('Nothing !');
		
		$mastodon_api = false;
		
		foreach($subs as $sub)
		{
			$sub->touch();
			try
			{
				switch($sub->platform)
				{
					case 'toot' :
						if($mastodon_api===false)
						{
							$mastodon_api = new Mastodon_api();
							$mastodon_api->set_url(APP_MASTODON_INSTANCE_URL);
							
							$mastodon_api->set_client(APP_MASTODON_CLIENT_ID,APP_MASTODON_CLIENT_SECRET);
							$mastodon_api->set_token(APP_MASTODON_ACCESS_TOKEN,APP_MASTODON_TOKEN_TYPE);
						}
						$this->_post_forecast_toot($sub, $mastodon_api);
						break;
					default :
						log::addlog('Unknown platform '.$sub->platform, true);
				}
			}
			catch(Exception $e)
			{
				log::addlog(print_r($sub), true);
				log::addlog((string) $e, true);
			}
		}
		return false;
	}
	
	
	/**
	 * post forecast in toot format
	 * 
	 * @params $sub : subscirption db object
	 */
	private function _post_forecast_toot($sub, &$mastodon_api)
	{
		$token = $sub->token;
		$prefs = json_decode($sub->preferences, true);
		
		if(!isset($prefs['emojis']))
			$prefs['emojis']=1;
		if(!isset($prefs['temperature']))
			$prefs['temperature']=1;
		if(!isset($prefs['rain3h']))
			$prefs['rain3h']=1;
		if(!isset($prefs['humidity']))
			$prefs['humidity']=0;
		if(!isset($prefs['wind']))
			$prefs['wind']=1;
		
		$forecast = owmap::get_forecast($sub->city_id);
		
		if($forecast===false)
		{
			log::addlog('Forecast not found for sub : '.print_r($sub), true);
		}
		
		$forecast = owmap::forecast_set_units($forecast, $sub->unit_temp, $sub->unit_speed, $sub->lang, $sub->timezone);
		
		$unsublink = "\n".APP_SITE_URL."/?/main/forecast/".$forecast->city->id."/-&unsub=".$token;
		
		$prevDay='';

		$status = $sub->address." ".$forecast->city->name."\n";
		if($prefs['emojis'])
			$status.="🌐 ";
		else
			$status.="tz=";
		$status.=$sub->timezone."\n";

		$isEven=true;
		foreach($forecast->list as $aForecast)
		{
			if($isEven)
			{
				$isEven=false;
				continue;
			}
			$isEven=true;
			$status_line="";
			if($aForecast->dt_local_long_day!=$prevDay)
			{
				$status_line .= "\n".$aForecast->dt_local_long_day."\n";
				$prevDay = $aForecast->dt_local_long_day;
			}
			
			$status_line .= $aForecast->dt_local_short_time;
			$status_line .= " ";

			if(count($aForecast->weather)>0)
			{
				foreach($aForecast->weather as &$weather)
				{
					if($prefs['emojis'])
						$status_line .= $weather->emoji;
					else
						$status_line .= $weather->local_description;
					$status_line .= " ";
				}
			}
			if($prefs['rain3h'] && $aForecast->rain->volume!='')
			{
				if($prefs['emojis'])
					$status_line .= str_replace('mm','㎜',$aForecast->rain->volume)." ";
				else
					$status_line .= $aForecast->rain->volume." ";
			}
			if($prefs['temperature'])
				$status_line .= $aForecast->main->temp." ";
			if($prefs['humidity'])
				$status_line .= $aForecast->main->humidity."%"." ";
			if($prefs['wind'])
			{
				if($prefs['emojis'])
					$status_line .= $aForecast->wind->emoji;
				else
					$status_line .= $aForecast->wind->dirtxt;
				$status_line .= " ";
				$status_line .= $aForecast->wind->speed;
			}
			$status_line .= "\n";
			
			$status_line = htmlspecialchars($status_line, ENT_XHTML, 'UTF-8');
			
			if(mb_strlen($status) + mb_strlen($status_line) + mb_strlen($unsublink) >= APP_MASTODON_MAX_TOOT_LEN)
				break;
			
			$status .= $status_line;
		}
		
		$status .= $unsublink;
		
		$res = $mastodon_api->post_statuses(array(
				'status' => $status,
				'visibility' => 'direct'
				));

		log::addlog(print_r($res, true), true);
		return false;
	}

}

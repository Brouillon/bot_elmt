<?php

class Get extends Controller
{
	/**
	 * Get forecast from token
	 *
	 * 	@params $args[0] = token
	 */
	function forecast($args)
	{
		$token = $args[0];
		
		// db connection
		$db = db::getDB();
		$db->insert('SET time_zone = "UTC"');
		
		$sub = $db->getFirst('SELECT s.*, c.name city_name, c.country city_country FROM subscriptions s
								LEFT OUTER JOIN cities c ON (c.id = s.city_id)
								WHERE token=?', array($token));
		
		if($sub===false)
			redirect('');
		
		$sub->touch();
		
		switch($sub->platform)
		{
			case 'ics' :
				return $this->_forecast_ics($sub);
			case 'rss' :
				return $this->_forecast_rss($sub);
		}
		
		redirect('main/forecast/'.$sub->city_id.'/'.txt::url($sub->city_name));
	}
	
	/**
	 * Get forecast in ics format
	 * 
	 * @params $sub : subsciption db object
	 */
	private function _forecast_ics($sub)
	{
		header('Content-Type: text/calendar; charset=UTF-8', true);
		
		$token = $sub->token;
		$prefs = json_decode($sub->preferences, true);
		
		if(!isset($prefs['emojis']))
			$prefs['emojis']=1;
		if(!isset($prefs['format']))
			$prefs['format']='digest';
		
		$forecast = owmap::get_forecast($sub->city_id);
		
		if($forecast===false)
			redirect('');
		
		$forecast = owmap::forecast_set_units($forecast, $sub->unit_temp, $sub->unit_speed, $sub->lang, $sub->timezone);
		
		
		echo "BEGIN:VCALENDAR\n";
		echo "PRODID:-//".APP_SITE_NAME."/NONSGML Bot Elmt ".BOTELMT_VERSION."//".strtoupper($sub->lang)."\n";
		echo "NAME:". $forecast->city->name.", ".$forecast->city->country."\n";
		echo "X-WR-CALNAME:". $forecast->city->name.", ".$forecast->city->country."\n";
		echo "URL:".APP_SITE_URL."/main/forecast/".$forecast->city->id."/".txt::url($forecast->city->name)."\n";
		echo "VERSION:2.0\n";
		
		if($prefs['format']=='digest')	/* One event per day */
		{
			$rDays = array();
			foreach($forecast->list as $aForecast)
				$rDays[$aForecast->dt_local_date] = $aForecast->dt_local_short_day;
				
			foreach($rDays as $dt_local_date => $day)
			{
				$dt_local_date_next = date('Y-m-d', strtotime($dt_local_date .' +1 day'));
				$summary=$day;
				$description=$forecast->city->name.", ".$forecast->city->country."\\n\\n\n ";
				if($prefs['emojis'])
					$description.="🌐 ";
				else
					$description.="TZ : ";
				$description.=$sub->timezone."\\n\\n\n ";
				
				$last_emoji="";
				$temp_min=false;
				$temp_max=false;
				foreach($forecast->list as $aForecast)
				{ 
					if($aForecast->dt_local_date!=$dt_local_date)
						continue;
					
					$description .= $aForecast->dt_local_short_time;
					$description .= " ";

					if(count($aForecast->weather)>0)
					{
						foreach($aForecast->weather as &$weather)
						{
							if($prefs['emojis'])
							{
								if($weather->emoji != $last_emoji)
									$summary .= " ".$weather->emoji;
								$last_emoji = $weather->emoji;
								
								$description .= $weather->emoji;
								$description .= " ";
							}
							$description .= $weather->local_description;
							$description .= " ";
						}
					}
					if($aForecast->rain->volume!='')
						$description .= '('.$aForecast->rain->volume.') ';
					if($temp_min==false || ($aForecast->main->temp*1.0)<$temp_min)
						$temp_min = $aForecast->main->temp;
					if($temp_max==false || ($aForecast->main->temp*1.0)>$temp_max)
						$temp_max = $aForecast->main->temp;
					$description .= $aForecast->main->temp;
					$description .= " ";
					$description .= $aForecast->main->humidity."%";
					$description .= " ";
					if($prefs['emojis'])
						$description .= $aForecast->wind->emoji;
					else
						$description .= $aForecast->wind->dirtxt;
					$description .= " ";
					$description .= $aForecast->wind->speed;
					$description .= "\\n\n ";
				}
				$summary .= " ".$temp_min."/".$temp_max;
				echo "BEGIN:VEVENT\n";
				echo "CREATED:".gmdate('Ymd')."T000000Z\n";
				echo "LAST-MODIFIED:".gmdate('Ymd\THis')."Z\n";
				echo "DTSTAMP:".gmdate('Ymd\THis')."Z\n";
				echo "UID:".$token."-".$dt_local_date."\n";
				echo "SUMMARY:".$summary."\n";
				echo "DTSTART;VALUE=DATE:".str_replace('-','',$dt_local_date)."\n";
				echo "DTEND;VALUE=DATE:".str_replace('-','',$dt_local_date_next)."\n";
				echo "TRANSP:TRANSPARENT\n";
				echo "LOCATION:".$forecast->city->name.", ".$forecast->city->country."\n";
				echo "DESCRIPTION:".trim($description)."\n";
				echo "END:VEVENT\n"; 
			}
		}
		else	/* One event per time range */
		{
			foreach($forecast->list as $idxF => $aForecast)
			{ 
				$summary="";
				$last_cond=false;
				$description = $forecast->city->name.", ".$forecast->city->country."\\n\\n\n ";
				$description .= $aForecast->dt_local_short_time;
				$description .= " ";
				if($prefs['emojis'])
					$description.="🌐 ";
				else
					$description.="TZ : ";
				$description.=$sub->timezone."\\n\\n\n ";
				
				if(count($aForecast->weather)>0)
				{
					foreach($aForecast->weather as &$weather)
					{
						if(($weather->emoji.$weather->local_description) == $last_cond)
							break;
						$last_cond = $weather->emoji.$weather->local_description;
						
						if($prefs['emojis'])
						{
							$summary .= $weather->emoji;
							$summary .= " ";
							$description .= $weather->emoji;
							$description .= " ";
						}
						$summary .= $aForecast->main->temp;
						$summary .= " ";
						$summary .= $weather->local_description;
						$summary .= " ";
						$description .= $weather->local_description;
						$description .= " ";
					}
				}
				if($aForecast->rain->volume!='')
					$description .= '('.$aForecast->rain->volume.') ';
				$description .= $aForecast->main->temp;
				$description .= " ";
				$description .= $aForecast->main->humidity."%";
				$description .= " ";
				if($prefs['emojis'])
					$description .= $aForecast->wind->emoji;
				else
					$description .= $aForecast->wind->dirtxt;
				$description .= " ";
				$description .= $aForecast->wind->speed;
				echo "BEGIN:VEVENT\n";
				echo "CREATED:".gmdate('Ymd')."T000000Z\n";
				echo "LAST-MODIFIED:".gmdate('Ymd\THis')."Z\n";
				echo "DTSTAMP:".gmdate('Ymd\THis')."Z\n";
				echo "UID:".$token."-".$aForecast->dt_utc_ics_from."\n";
				echo "SUMMARY:".$summary."\n";
				echo "DTSTART:".$aForecast->dt_utc_ics_from."\n";
				echo "DTEND:".$aForecast->dt_utc_ics_to."\n";
				echo "TRANSP:TRANSPARENT\n";
				echo "LOCATION:".$forecast->city->name.", ".$forecast->city->country."\n";
				echo "DESCRIPTION:".trim($description)."\n";
				echo "END:VEVENT\n"; 
			}
			
		}
		
		echo "END:VCALENDAR\n";
		return false;
	}
	
	/**
	 * Get forecast in rss feed format
	 *
	 * @params $sub : subsciption db object
	 */
	private function _forecast_rss($sub)
	{
		header('Content-Type: application/rss+xml; charset=UTF-8', true);

		$token = $sub->token;
		$prefs = json_decode($sub->preferences, true);
		
		if(!isset($prefs['emojis']))
			$prefs['emojis']=1;
		
		$forecast = owmap::get_forecast($sub->city_id);
		
		if($forecast===false)
			redirect('');
		
		$forecast = owmap::forecast_set_units($forecast, $sub->unit_temp, $sub->unit_speed, $sub->lang, $sub->timezone);
		
		
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		echo "<rss version=\"2.0\">\n";
		echo "	<channel>\n";
		echo "		<title>". $forecast->city->name.", ".$forecast->city->country."</title>\n";
		echo "		<description></description>\n";
		echo "		<lastBuildDate>".date('r')."</lastBuildDate>\n";
		echo "		<link>".APP_SITE_URL."/?/main/forecast/".$sub->city_id."/".txt::url($forecast->city->name)."</link>\n";
		
		$rDays = array();
		foreach($forecast->list as $aForecast)
			$rDays[$aForecast->dt_local_date] = $aForecast->dt_local_date;
			
		foreach($rDays as $dt_local_date => $day)
		{
			$first_dt_local_2822=false;
			$summary=$forecast->city->name." / ".$day;
			$description="<p>TZ : ".$sub->timezone."</p>";
			$last_emoji="";
			$temp_min=false;
			$temp_max=false;
			foreach($forecast->list as $aForecast)
			{ 
				if($aForecast->dt_local_date!=$dt_local_date)
					continue;
				
				if($first_dt_local_2822===false)
					$first_dt_local_2822 = $aForecast->dt_local_2822;
				$description .= "<p>";
				$description .= $aForecast->dt_local_short_time;
				$description .= " ";

				if(count($aForecast->weather)>0)
				{
					foreach($aForecast->weather as &$weather)
					{
						if($prefs['emojis'])
						{
							if($weather->emoji != $last_emoji)
								$summary .= " ".$weather->emoji;
							$last_emoji = $weather->emoji;
						}
						$description .= '<img src="'.$weather->img.'" width="20" height="20" />';
						$description .= " ";
						$description .= $weather->local_description;
						$description .= " ";
					}
				}
				if($aForecast->rain->volume!='')
					$description .= '('.$aForecast->rain->volume.') ';
				if($temp_min==false || ($aForecast->main->temp*1.0)<$temp_min)
					$temp_min = $aForecast->main->temp;
				if($temp_max==false || ($aForecast->main->temp*1.0)>$temp_max)
					$temp_max = $aForecast->main->temp;
				$description .= $aForecast->main->temp;
				$description .= " ";
				$description .= $aForecast->main->humidity."%";
				$description .= " ";
				if($prefs['emojis'])
					$description .= $aForecast->wind->emoji;
				else
					$description .= $aForecast->wind->dirtxt;
				$description .= " ";
				$description .= $aForecast->wind->speed;
				$description .="</p>";
			}
			$summary .= " ".$temp_min."/".$temp_max;
			
			echo "		<item>\n";
			echo "			<title>".$summary."</title>\n";
			echo "			<description>".htmlspecialchars($description, ENT_QUOTES|ENT_HTML5, 'UTF-8')."</description>\n";
			echo "			<pubDate>".$first_dt_local_2822."</pubDate>\n";
			echo "			<link>".APP_SITE_URL."/?/main/forecast/".$sub->city_id."/".txt::url($forecast->city->name)."/".txt::url($dt_local_date)."</link>\n";
			echo "			<guid isPermaLink=\"true\">".APP_SITE_URL."/?/main/forecast/".$sub->city_id."/".txt::url($forecast->city->name)."/".txt::url($dt_local_date)."</guid>\n";
			echo "		</item>\n";
		}
		
		echo "	</channel>\n";
		echo "</rss>\n";
		return false;
	}
	
}

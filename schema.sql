 
CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `country` varchar(2) NOT NULL,
  `lon` decimal(10,7) NOT NULL,
  `lat` decimal(10,7) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='List of known cities';

CREATE TABLE IF NOT EXISTS `subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(32) NOT NULL,
  `status` varchar(1) NOT NULL,
  `platform` varchar(10) NOT NULL,
  `preferences` TEXT NOT NULL,
  `address` varchar(64) NOT NULL,
  `city_id` int(11) NOT NULL,
  `lang` varchar(2) NOT NULL,
  `unit_temp` varchar(1) NOT NULL,
  `unit_speed` varchar(10) NOT NULL,
  `timezone` varchar(32) NOT NULL,
  `local_time_range` int(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
   PRIMARY KEY (`id`),
   UNIQUE KEY `token` (`token`)
)  ENGINE=innoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='User subscriptions';

CREATE TABLE IF NOT EXISTS `forecasts` (
  `id` int(11) NOT NULL,
  `forecast_json` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=innoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='Cities forecasts';

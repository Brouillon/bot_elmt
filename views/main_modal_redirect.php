							<div class="modal-dialog  ctrl_<?=$_ctrl?> view_<?=$_viewpage?>" role="document">
									<div class="modal-content dark-bg">
										<div class="modal-header">
											<h5 class="modal-title"><i class="fa fa-fw fa-lg fa-random"></i> <?=t('Redirection')?></h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<div class="contact-form">
												<?php if(isset($message)) { ?>
												<p class="alert alert-icon alert-warning" role="alert"><i class="fa fa-warning"></i>
													<?=$message?>
												</p>
												<?php } ?>
												<p><?=t('Please_wait')?></p>
												<p align="center"><img src="../assets/img/bx_loader.gif"></p>
											</div>
										</div>
									</div>
								</div>

								<script>
									window.location.href = "<?=$redirecturl?>";
								</script>
